/*
===============================================================================
 Name        : AccelTest.c
 Author      : $(author)
 Version     : 0.1
 Copyright   : Copyright (c) 2019 Alexander Zhang. All rights reserved.
 Description : ADXL343 Functions
===============================================================================
*/

#include "board.h"

/* 7-bit I2C addresses of ADXL343 */
/* Note: The ROM code requires the address to be between bits [6:0]
         bit 7 is ignored */
#define I2C_ADDR_ADXL343 (0x1D)
#define I2C_ADDR_ALT343  (0x53)

/* ADXL343 REGISTER MAP (Table 19 in datasheet)
        Name                       Address    Type  Reset Value   Description */
#define ADXL343_REG_DEVID          0x00     // R      11100101    Device ID
#define ADXL343_REG_THRESH_TAP     0x1D     // R/W    00000000    Tap threshold
#define ADXL343_REG_OFSX           0x1E     // R/W    00000000    X-axis offset
#define ADXL343_REG_OFSY           0x1F     // R/W    00000000    Y-axis offset
#define ADXL343_REG_OFSZ           0x20     // R/W    00000000    Z-axis offset
#define ADXL343_REG_DUR            0x21     // R/W    00000000    Tap duration
#define ADXL343_REG_LATENT         0x22     // R/W    00000000    Tap latency
#define ADXL343_REG_WINDOW         0x23     // R/W    00000000    Tap window
#define ADXL343_REG_THRESH_ACT     0x24     // R/W    00000000    Activity threshold
#define ADXL343_REG_THRESH_INACT   0x25     // R/W    00000000    Inactivity threshold
#define ADXL343_REG_TIME_INACT     0x26     // R/W    00000000    Inactivity time
#define ADXL343_REG_ACT_INACT_CTL  0x27     // R/W    00000000    Axis enable control for activity and inactivity detection
#define ADXL343_REG_THRESH_FF      0x28     // R/W    00000000    Free-fall threshold
#define ADXL343_REG_TIME_FF        0x29     // R/W    00000000    Free-fall time
#define ADXL343_REG_TAP_AXES       0x2A     // R/W    00000000    Axis control for single tap/double tap
#define ADXL343_REG_ACT_TAP_STATUS 0x2B     // R      00000000    Source of single tap/double tap
#define ADXL343_REG_BW_RATE        0x2C     // R/W    00001010    Data rate and power mode control
#define ADXL343_REG_POWER_CTL      0x2D     // R/W    00000000    Power-saving features control
#define ADXL343_REG_INT_ENABLE     0x2E     // R/W    00000000    Interrupt enable control
#define ADXL343_REG_INT_MAP        0x2F     // R/W    00000000    Interrupt mapping control
#define ADXL343_REG_INT_SOURCE     0x30     // R      00000010    Source of interrupts
#define ADXL343_REG_DATA_FORMAT    0x31     // R/W    00000000    Data format control
#define ADXL343_REG_DATAX0         0x32     // R      00000000    X-Axis Data 0
#define ADXL343_REG_DATAX1         0x33     // R      00000000    X-Axis Data 1
#define ADXL343_REG_DATAY0         0x34     // R      00000000    Y-Axis Data 0
#define ADXL343_REG_DATAY1         0x35     // R      00000000    Y-Axis Data 1
#define ADXL343_REG_DATAZ0         0x36     // R      00000000    Z-Axis Data 0
#define ADXL343_REG_DATAZ1         0x37     // R      00000000    Z-Axis Data 1
#define ADXL343_REG_FIFO_CTL       0x38     // R/W    00000000    FIFO control
#define ADXL343_REG_FIFO_STATUS    0x39     // R      00000000    FIFO status

/* 7-bit I2C addresses of STMPE811 */
/* Note: The ROM code requires the address to be between bits [6:0]
         bit 7 is ignored */
#define I2C_ADDR_STMPE811 (0x41)
#define I2C_ADDR_ALT811   (0x44)

/* ADXL343 REGISTER MAP (Table 19 in datasheet)
        Name                   Address     Type  Reset Value  Description */
#define STMPE811_REG_CHIP_ID   0x00        // R    0x0811       Device identification
#define STMPE811_REG_ID_VER    0x02        // R    0x03        Revision number

/* I2CM transfer record */
static I2CM_XFER_T i2cmXferRec;

static uint8_t txData[4];
static uint8_t rxData[4];

STATIC INLINE void ADXL343_PrintErr(uint8_t code)
{
	Board_UARTPutSTR("[I2CM_STATUS_");
	if (code == 0x00) Board_UARTPutSTR("OK");
	if (code == 0x01) Board_UARTPutSTR("ERROR");
	if (code == 0x02) Board_UARTPutSTR("NAK_ADR");
	if (code == 0x03) Board_UARTPutSTR("BUS_ERROR");
	if (code == 0x04) Board_UARTPutSTR("NAK_DAT");
	if (code == 0x05) Board_UARTPutSTR("ARBLOST");
	Board_UARTPutChar(']');
}

/* Function to setup and execute I2C transfer request */
STATIC INLINE void SetupXferRecAndExecute(uint8_t devAddr, uint8_t *txBuffPtr, uint16_t txSize, uint8_t *rxBuffPtr, uint16_t rxSize)
{
	/* Setup I2C transfer record */
	i2cmXferRec.slaveAddr = devAddr;
	i2cmXferRec.status = 0;
	i2cmXferRec.txSz = txSize;
	i2cmXferRec.rxSz = rxSize;
	i2cmXferRec.txBuff = txBuffPtr;
	i2cmXferRec.rxBuff = rxBuffPtr;

	Chip_I2CM_XferBlocking(LPC_I2C, &i2cmXferRec);

	ADXL343_PrintErr(i2cmXferRec.status);
}

STATIC INLINE uint8_t ADXL343_ReadReg(uint8_t devAddr, uint8_t reg)
{
	txData[0] = reg;
	SetupXferRecAndExecute(devAddr, txData, 1, rxData, 1);
	return rxData[0];
}

STATIC INLINE void ADXL343_WriteReg(uint8_t devAddr, uint8_t reg, uint8_t val)
{
	txData[0] = reg;
	txData[1] = val;
	SetupXferRecAndExecute(devAddr, txData, 2, rxData, 0);
}
