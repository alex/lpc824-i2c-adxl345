/*
===============================================================================
 Name        : AccelTest.c
 Author      : $(author)
 Version     : 0.1
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "board.h"

#include <cr_section_macros.h>

#include "adxl343.h"

#include <string.h>

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/* System clock is set to 24MHz, I2C clock is set to 600kHz */
#define I2C_CLK_DIVIDER         (40)
/* 100KHz I2C bit-rate */
#define I2C_BITRATE             (100000)

/* SysTick rate in Hz */
#define TICKRATE_HZ             (1000)

#define TASK_LOOP               while (true)
#define EVENT_READ_ACCEL        0x01
#define EVENT_LED_TOGGLE        0x02

/* file local variables */
static volatile uint32_t ticks;
static uint32_t event_flag;

static int16_t value;
static char		out_str[16];

/* UART handle and memory for ROM API */
static UART_HANDLE_T *uartHandle;

/* Use a buffer size larger than the expected return value of
   uart_get_mem_size() for the static UART handle type */
static uint32_t uartHandleMEM[0x10];

/* Receive buffer */
#define RECV_BUFF_SIZE 32
static char recv_buf[RECV_BUFF_SIZE];

/* ASCII code for escape key */
#define ESCKEY 27
#define UART_CLOCK_DIV 1

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

union datu {
	int i;
	unsigned u;
} data;

/*****************************************************************************
 * Private functions
 ****************************************************************************/
#define	LED_RED	  0
#define	LED_GREEN 1
#define	LED_BLUE  2
INLINE STATIC void Board_LED_clear(void)
{
	Board_LED_Set(LED_RED, false);
	Board_LED_Set(LED_GREEN, false);
	Board_LED_Set(LED_BLUE, false);
}

/* Initializes pin muxing for I2C interface */
INLINE STATIC void Init_PinMux(void)
{
	/* Enable the clock to the Switch Matrix */
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_SWM);

	/* 824 needs the fixed pin ACMP2 pin disabled to use pin as gpio */
	Chip_SWM_DisableFixedPin(SWM_FIXED_CLKIN);

	/* Enable Fast Mode Plus for I2C pins */
	Chip_SWM_EnableFixedPin(SWM_FIXED_I2C0_SDA);
	Chip_SWM_EnableFixedPin(SWM_FIXED_I2C0_SCL);
	Chip_IOCON_PinSetI2CMode(LPC_IOCON, IOCON_PIO10, PIN_I2CMODE_FASTPLUS);
	Chip_IOCON_PinSetI2CMode(LPC_IOCON, IOCON_PIO11, PIN_I2CMODE_FASTPLUS);

	/* Disable the clock to the Switch Matrix to save power */
	Chip_Clock_DisablePeriphClock(SYSCTL_CLOCK_SWM);
}

/* Setup I2C handle and parameters */
INLINE STATIC void setupI2CMaster(void)
{
	Chip_I2C_Init(LPC_I2C);
	Chip_I2C_SetClockDiv(LPC_I2C, I2C_CLK_DIVIDER);
	Chip_I2CM_SetBusSpeed(LPC_I2C, I2C_BITRATE);
	Chip_I2CM_Enable(LPC_I2C);
}

INLINE STATIC void ANSI_ClrScrn(void)
{
	Board_UARTPutSTR("\x1B[2J");   // clear screen
	Board_UARTPutSTR("\x1B[0m");   // color mode reset
	Board_UARTPutSTR("\x1B[1;1H"); // set position to 1,1
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/**
 * @brief	Handle interrupt from SysTick timer
 * @return	Nothing
 */
__RAMFUNC(RAM) void SysTick_Handler(void)
{
	ticks++;

	if ((ticks % 500) == 0) event_flag |= EVENT_LED_TOGGLE;

	if ((ticks % 50) == 0) event_flag |= EVENT_READ_ACCEL;
}

/**
 * @brief	Main routine for I2CM example
 * @return	Function should not exit
 */
int main(void)
{
	/* Generic Initialization */
	SystemCoreClockUpdate();
	Board_Init();

	/* Clear LED and console */
	Board_LED_clear();
	ANSI_ClrScrn();

	/* Startup spew */
	Board_UARTPutSTR("Starting...\r\n");
	Board_UARTPutSTR("ADXL343 Accelerometer Testing Program\r\n");
	Board_UARTPutSTR("Version: 0.1.1945a\r\n");
	Board_UARTPutSTR("Build: " __DATE__ " "  __TIME__ "\r\n");
	Board_UARTPutSTR("MCU: LPC824\r\n");
	Board_UARTPutSTR("File: " __FILE__ "\r\n");

	/* Setup I2C */
	Init_PinMux();
	setupI2CMaster();
	NVIC_DisableIRQ(I2C_IRQn);

	/* Enable SysTick Timer */
	SysTick_Config(SystemCoreClock / TICKRATE_HZ);

	/* Set up prompt */
	char input_buf[64];


	Board_UARTPutChar('\n');
	Board_UARTPutSTR("Initializing ADXL343 at address I2C_ADDR_ALT343 (0x53)\r\n");
	Board_UARTPutSTR("Attempting to identify device...\r\n");

	Board_UARTPutSTR("Read: I2C_ADDR_STMPE811:");
	//value = ADXL343_ReadReg(I2C_ADDR_ALT343, ADXL343_REG_DEVID);
	value = ADXL343_ReadReg(I2C_ADDR_STMPE811, STMPE811_REG_CHIP_ID);
	Board_itoa(value, out_str, 16);
	Board_UARTPutSTR("0x");
	Board_UARTPutSTR(out_str);
	Board_UARTPutSTR("\r\n");
	if (value != 0xE5) {
		Board_UARTPutSTR("\nFATAL: Unable to identify ADXL343!");
		//while (1);
	}
	Board_UARTPutSTR("Device ID confirmed.\r\n");

	/* Place the ADXL343 in measurement mode
	ADXL343_WriteReg(I2C_ADDR_ALT343, ADXL343_REG_POWER_CTL, 0x08);
	/* Set measurement range
	ADXL343_WriteReg(I2C_ADDR_ALT343, ADXL343_REG_DATA_FORMAT, 0x0C);
	ADXL343_WriteReg(I2C_ADDR_ALT343, ADXL343_REG_BW_RATE, 0x06);*/

	Board_UARTPutSTR("Entering task loop.\r\n\n");

	/* Enter the task loop */
	int weow;
	TASK_LOOP {
		//__WFI(); Don't WFI because of UART stuff

		while ((weow = Board_UARTGetChar()) != EOF) {
			Board_itoa(weow, out_str, 10);
			Board_UARTPutChar((uint8_t) weow);
		}

		/* Toggle on-board RED LED to show activity. */
		if (event_flag & EVENT_LED_TOGGLE) {
			event_flag &= ~EVENT_LED_TOGGLE;
			Board_LED_Toggle(0);

		}

		if (event_flag & EVENT_READ_ACCEL) {
			event_flag &= ~EVENT_READ_ACCEL;
			//Board_UARTPutSTR("Read: ADXL343_REG_DATAZ1:");

			//value = ((int16_t) ADXL343_ReadReg(I2C_ADDR_ALT343, ADXL343_REG_DATAZ1)) << 8 + ADXL343_ReadReg(I2C_ADDR_ALT343, ADXL343_REG_DATAZ0);
			//value = (~value) + 1;

			//Board_itoa(value, out_str, 10);
			//Board_UARTPutSTR(out_str);
			//Board_UARTPutSTR("\r\n");

			data.u = ADXL343_ReadReg(0x41, 0);

			Board_itoa(data.u, out_str, 10);

			Board_UARTPutSTR(out_str);


			Board_UARTPutSTR("\r\n");
		}


		/* Set output value on port 0 pins 0-3 (0-1 on the 824). Bits that are
		   not enabled by the mask are ignored when setting the port value */
		//Chip_GPIO_SetMaskedPortValue(LPC_GPIO_PORT, 0, count);


#define PORT_MASK       0x3
	/* Set port 0 pins 0-3 (0-1 on 824) to the output direction*/
	//Chip_GPIO_SetPortDIROutput(LPC_GPIO_PORT, 0, PORT_MASK);
	/* Set GPIO port mask value to make sure only port 0
	   pins 0-3 (0-1 on 824) are active during state change */
	//Chip_GPIO_SetPortMask(LPC_GPIO_PORT, 0, ~PORT_MASK);

	}
}
